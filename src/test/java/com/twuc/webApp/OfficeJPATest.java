package com.twuc.webApp;

import com.twuc.webApp.demo.Office;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
class OfficeJPATest {
    @Autowired
    private OfficeJPA repo;
    @Autowired
    private EntityManager em;

    @Test
    void name() {
        assertTrue(true);
    }

    @Test
    void should_save_office() {
        Office xian = repo.save(new Office(1L, "xian"));
        em.flush();
        assertNotNull(xian);
    }

    @Test
    void should_persist_office() {
        em.persist(new Office(1L,"wuhan"));
        Office office = em.find(Office.class, 1L);
        em.flush();
        assertEquals(Long.valueOf(1),office.getId());
        assertEquals("wuhan",office.getCity());
    }
}