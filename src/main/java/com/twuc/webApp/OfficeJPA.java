package com.twuc.webApp;

import com.twuc.webApp.demo.Office;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfficeJPA extends JpaRepository<Office,Long> {


}
